'use strict';

class Routes {


    constructor(app, socket) {
        this.app = app;
        this.io = socket;
    }


    appRoutes() {
        this.app.get('/', (req, res) => {
            res.send(req.ip);
        });
    }

    socketEvents() {
        this.io.on('connection', (socket) => {
            console.log('a user connected with sockedId ', socket.id);
            socket.on('trackingPosition', (data) => {
                // the data includes the userId, and latitude and longitude
                console.log(Date.now(), data)
            });

            socket.on('test', (data) => {
                // the data includes the userId, and latitude and longitude
                console.log("test", data)
            });


            socket.on('disconnect', () => {

            });

        })

    }



    routesConfig() {
        this.appRoutes();
        this.socketEvents();
    }



}




module.exports = Routes;




