'use strict';

const express = require("express");
const http = require('http');
const socketio = require('socket.io');
const bodyParser = require('body-parser');
require('dotenv').config(); //this allow us read env vars defined in .env file
const routes = require('./routes')



class Server {


    constructor() {
        this.port = process.env.PORT;//define port
        this.app = express();//create a express
        //create a socket
        this.http = http.Server(this.app);
        this.socket = socketio(this.http);
    }


    appConfig() {
        //parse incoming string as json (TO API CALLS)
        this.app.use(
            bodyParser.json()
        );
    }


    /* Including app Routes starts*/
    includeRoutes() {
        //pass as params the express app and the socket
        new routes(this.app, this.socket).routesConfig();
    }
    /* Including app Routes ends*/




    appExecute() {
        this.appConfig();
        this.includeRoutes();//iclude routes to the server
        this.http.listen(this.port, () => {
            console.log(`Listening on ${this.port}`);
        });
    }

}



const app = new Server();
app.appExecute();