//we recomend don't use var, use const when you import a library or function, this hepls to improve performance, 
const express = require("express");
const bodyParser = require('body-parser');

const app = express();//create an express app

require('dotenv').config(); //this allow us read env vars defined in .env file

//this allow us parse strings to object since the resquests
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());


const http = require('http').Server(app);
const io = require('socket.io')(http);



//use routes
// pass express app as a param
require('./routes/auth')(app)
require('./routes/push-notifications')(app)
//add more routes as you like




//finally launch the server
app.listen(process.env.PORT, () => {
    console.log(`👌 Good! Server is running on ${process.env.PORT}`)
})