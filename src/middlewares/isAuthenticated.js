const jwt = require('jsonwebtoken');



//I'm using 'async' because jwt work asynchronous
module.exports = async (req, res, next) => {
    //use try and catch to cath errors when the token is invalid
    try {
        const token = req.headers['x-access-token'] //get token from headers
        if (token) {//id the token is not null
            const { userId } = await jwt.verify(token, process.env.APP_SECRET)
            if (userId) {
                next()
            } else {
                res.status(403).send({ status: 403, message: 'The provided token has been expired' });
            }
        } else {
            res.status(403).send({ status: 403, message: 'Plase provide an access token' });
        }
    } catch (error) {
        res.status(500).send({ status: 500, message: error.message })
    }
}