const jwt = require('jsonwebtoken');

const isAuthenticated = require('../../middlewares/isAuthenticated');


const route_path = "/api/users";

const phoneNumberDemo = "+593984357941";

//use arrow functions to get a better code
module.exports = (app) => {


    //route to do a login
    app.post(`${route_path}/login`, async (req, res) => {
        const { phoneNumber } = req.body;//get phone number from request params
        if (!phoneNumber) return res.status(403).send({ status: 403, message: 'Número de celular invalido' })

        if (phoneNumberDemo !== phoneNumber) return res.status(403).send({ status: 403, message: 'Este número de celular no consta en nuestra base de datos' })

        //create a user object only  with the public information
        const userId = "lalalallalalala";

        //create a jwt token with mock data
        const token = await jwt.sign({ userId }, process.env.APP_SECRET, { expiresIn: '1d' })//jwt token that expires in  1 day, to 10 minutes use 10m

        //return the status, token and user data
        res.send({
            status: 200,
            token,
        });
    })




    //route to get all info user
    app.post(`${route_path}/get-info`, isAuthenticated, async (req, res) => {
        
        //return a complete user info
        res.send({
            status: 200,
            user: { ...user, addr: "Quito 170505" }// ...user allow to descruture user json and create a new json joined with addr
        });


    })





}