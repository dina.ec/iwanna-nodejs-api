const isAuthenticated = require('../middlewares/isAuthenticated')
const request = require('request');

module.exports = (app) => {

    app.post('/call-center/send-push-notification', isAuthenticated, async (req, res, next) => {

        const { to, registration_ids, body, title, extraData } = req.body
        //body, title  are required

        //by default send the notification to all devices
        var data = {
            notification: {
                body,
                title
            },
            data: {
                body,
                title,
                ...extraData
            }
        };


        if (to) {//send notification to specific device by token or an specific topic
            data = { to, ...data }
        } else if (registration_ids) {//send notification to multiples devices by an array of tokens
            data = { registration_ids, ...data }
        }



        request.post({
            url: "https://fcm.googleapis.com/fcm/send",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `key=${process.env.FCM_KEY}`
            },
            body: JSON.stringify(data)
        }, function (err, response, body) {
            if (err) {
                return res.status(500).send({ status: 500, message: err.message })
            }
            console.log(body)
            res.status(200).send({ status: 200, message: "ok" })
        })

    })

}